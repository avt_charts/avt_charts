(function() {

  'use strict';

  angular.module('hiberus-charts').directive('hbRoundProgressColumn', hbRoundProgressColumnDirective);

  function hbRoundProgressColumnDirective() {
    return {
      restrict: 'E',
      link:  {post: Link
            },
      template: '<div class="round-parent-wrapper">'+ 
                  '<div class="round-wrapper round-wrapper-{{length}}"  ng-repeat-n="length">'+
                    '<div class="text" ng-if="valuesRoundProgress[$index].value != null">'+
                      '{{valuesRoundProgress[$index].value>0?\'+\':\'\'}}{{valuesRoundProgress[$index].value | number:0}}%'+
                    '</div>'+
                    '<div ng-if="valuesRoundProgress[$index].value != null" animation-delay="2000" clockwise="valuesRoundProgress[$index].value > 0" color="{{valuesRoundProgress[$index].color}}" current="valuesRoundProgress[$index].value > 0 ? valuesRoundProgress[$index].value : - valuesRoundProgress[$index].value  " fill="#FFFFFF" fill-opacity="0.5" max="100" min="0" offset="0" radius="25" responsive="responsive" round-progress="" rounded="rounded" stroke="5">'+
                    '</div>'+
                  '</div>'+
                '</div>' ,
      scope: {
        valuesRoundProgress           : '=',
        length                        : '='
      },
      replace: true
    };
  }


  function Link (scope, element, attrs) {
  }

})();