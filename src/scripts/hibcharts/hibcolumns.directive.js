(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibColumns', HibColumnsDirective);
	
	HibColumnsDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	function HibColumnsDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			scope: {
				animationMs: '=',
				background: '@',
				categories: '=',
				colors: '=',
				colorByPoint: '=',
				labelsDataSeries: '=',
				heightAuto: '=',
				isRoundProgress: '=',
				hbLegend: '=',
				labelFormatter: '=',
				noBorder: '=',
				noMargin: '=',
				marker: '=',
				onSelect: '&',
				onUnselect: '&',
				pageSize: '=',
				paginable: '=',
				position: '@',
				rotatedLabels: '=',
				selectable: '=',
				selectedPoint: '=',
				series: '=',
				stack: '=',
				symbolRadius: '=',
				tooltip: '=',
				type: '=',
				axisXLineWidth: '=',
				axisXTickWidth: '=',
				yAxisLabelsHidden: '=',
				yAxisTitle: '@',
				yAxisMax: '=',
				yAxisLineWidth: '=',
				yAxisGridLineWidth: '=',
				zoomType: '=',
				vAxisTitle: '@',
				axisColorX: '=',
				axisColorY: '=',
				gridColorX: '=',
				gridColorY: '=',
				tickLength: '=',
				tickColor: '='
			},
			link: HibColumnsDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			'<hb-round-progress-column ng-if="initRoundProgress" length="length"  values-round-progress="valuesRoundProgress"></hb-round-progress-column>' +
			'<div></div>' +
			'<div class="button-wrapper type-arrow" ng-if="paginable">' +
			'<a href="" ng-click="PageLeft()"><i class="brand-icon brand-icon-arrow prev" ng-class="{ inactive : !canPageLeft }"></i></a>' +
			'<a href="" ng-click="PageRight()"><i class="brand-icon brand-icon-arrow next" ng-class="{ inactive : !canPageRight }"></i></a>' +
			'</div>' +
			'<div ng-if="loading" class="no-data no-data-columns"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function HibColumnsDirectiveLink(scope, element) {
			var chartElement = element[0].lastChild.firstChild.nextSibling;
			var noDataElement = element[0].lastChild.lastChild;
			var page = 0;
			var chart = null;
			
			scope.canPageLeft = false;
			scope.canPageRight = false;
			scope.PageRight = function () {
				if (scope.canPageRight) {
					page += 1;
					Build();
				}
			};
			scope.PageLeft = function () {
				if (scope.canPageLeft) {
					page -= 1;
					Build();
				}
			};
			
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			scope.$watch('stack', function () {
				Build();
			});
			scope.$watch('selectedPoint', function () {
				if (!SelectPoint()) {
					PaginateToSelected();
				}
			});
			
			///////
			
			function Build() {
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				scope.loading = scope.series["loading"];
				
				var series = JSON.parse(JSON.stringify(scope.series));
				var categories = JSON.parse(JSON.stringify(scope.categories));
				scope.initRoundProgress = false;
				scope.valuesRoundProgress = [];
				
				if (scope.paginable) {
					var pageSize = scope.pageSize ? scope.pageSize : 10;
					var start = page * pageSize;
					var end = start + pageSize;
					var dataLength = hasData ? series[0].data.length : 0;
					
					scope.canPageLeft = page > 0;
					scope.canPageRight = end < dataLength;
					
					for (var i = 0; i < series.length; i += 1) {
						series[i].data = series[i].data.slice(start, end);
					}
					categories = categories.slice(start, end);
				}
				
				scope.length = categories.length;
				
				if (scope.isRoundProgress && series[1] != null) {
					scope.initRoundProgress = true;
					_.forEach(categories, function (value, index) {
						scope.valuesRoundProgress.push({
							value: series[1].data[index] != null && series[0].data[index] > 0 && series[1].data[index] > 0 ? ((series[1].data[index] - series[0].data[index]) / series[0].data[index]) * 100 : null,
							color: series[1].data[index] - series[0].data[index] > 0 ? '#0088ec' : 'red'
						});
					});
					
				}
				
				
				var seriesOptions = {
					animation: {duration: scope.animationMs ? scope.animationMs : 1000},
					colorByPoint: scope.colorByPoint ? true : false,
					dataLabels: {
						enabled: scope.labelsDataSeries ? true : false,
					},
					point: {
						events: {
							select: OnSelectHandler,
							unselect: OnUnselectHandler
						}
					},
					borderWidth: scope.noBorder ? 0 : 1,
					states: {select: HibchartsOptions.columnsSelectedStyle},
					cursor: scope.selectable ? 'pointer' : '',
					allowPointSelect: scope.selectable ? true : false,
					marker: {
						enabled: scope.marker ? true : false
					}
				};
				
				var options = {
					series: series,
					lang: {
						noData: "No Data Display"
					},
					xAxis: {
						categories: categories,
						lineWidth: !scope.axisXLineWidth && typeof scope.axisXLineWidth !== 'undefined' ? scope.axisXLineWidth : 1,
						tickWidth: !scope.axisXTickWidth && typeof scope.axisXTickWidth !== 'undefined' ? scope.axisXTickWidth : 1,
						labels: {
							rotation: scope.rotatedLabels ? -45 : 0,
							useHTML: true,
							formatter: scope.labelFormatter ? scope.labelFormatter : undefined
						},
						lineColor: scope.axisColorX ? scope.axisColorX : '#ccd6eb',
						gridLineColor: scope.gridColorX ? scope.gridColorX : '#e6e6e6',
						tickLength: scope.tickLength ? scope.tickLength : 10,
						tickColor: scope.tickColor ? scope.tickColor : '#ccd6eb'
					},
					yAxis: {
						title: {text: scope.yAxisTitle},
						max: !scope.yAxisMax ? null : scope.yAxisMax,
						lineWidth: !scope.yAxisLineWidth && typeof scope.yAxisLineWidth !== 'undefined' ? scope.yAxisLineWidth : 1,
						gridLineWidth: !scope.yAxisGridLineWidth && typeof scope.yAxisGridLineWidth !== 'undefined' ? scope.yAxisGridLineWidth : 1,
						labels: {
							enabled: scope.yAxisLabelsHidden ? false : true,
						},
						lineColor: scope.axisColorY ? scope.axisColorY : '#ccd6eb',
						gridLineColor: scope.gridColorY ? scope.gridColorY : '#e6e6e6'
					},
					title: {text: ''},
					colors: scope.colors ? scope.colors : HibchartsOptions.defaultColors,
					chart: {
						type: scope.type ? scope.type : 'column',
						backgroundColor: scope.background ? scope.background : 'transparent',
						marginTop: scope.noMargin ? null : scope.isRoundProgress ? 20 : 50,
						marginLeft: scope.noMargin && !scope.paginable ? null : scope.paginable && scope.noMargin ? 70 : scope.paginable ? 85 : 70,
						marginRight: scope.noMargin && !scope.paginable ? null : scope.position === 'lateral' ? 100 : scope.noMargin && scope.paginable ? 25 : 50,
						marginBottom: scope.noMargin ? null : scope.position === 'lateral' ? 50 : 100,
						height: scope.heightAuto ? null : scope.isRoundProgress && scope.initRoundProgress ? 350 : 400,
						zoomType: scope.zoomType ? scope.zoomType : null,
					},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {
						enabled: scope.hbLegend ? true : false,
						layout: scope.position === 'lateral' ? 'vertical' : 'horizontal',
						verticalAlign: scope.position === 'lateral' ? 'middle' : 'bottom',
						align: scope.position === 'lateral' ? 'right' : 'center',
						itemStyle: HibchartsOptions.legendItemStyle,
						maxHeight: 80,
						symbolHeight: scope.symbolRadius ? 6 : null,
						symbolWidth: scope.symbolRadius ? 6 : null,
						symbolRadius: scope.symbolRadius ? 6 : null,
						navigation: {
							activeColor: '#3E576F',
							animation: true,
							arrowSize: 12,
							inactiveColor: '#CCC',
							style: {
								fontWeight: 'bold',
								color: '#333',
								fontSize: '12px'
							}
						}
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: seriesOptions,
						column: {stacking: scope.stack ? 'normal' : false}
					}
				};
				
				
				chart = Highcharts.chart(chartElement, options);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				SelectPoint();
			}
			
			function SelectPoint() {
				if (scope.selectedPoint !== undefined && chart !== null && chart.series.length > 0) {
					for (var i = 0; i < chart.series[0].data.length; i += 1) {
						if (chart.series[0].data[i].category === scope.selectedPoint) {
							chart.series[0].data[i].select(true, false);
							return true;
						}
						else if (scope.selectedPoint === null) {
							if (chart.series[0].data[i].selected) {
								chart.series[0].data[i].select(false);
							}
						}
					}
				}
				
				return false;
			}
			
			function PaginateToSelected() {
				if (scope.paginable && scope.selectedPoint !== undefined) {
					for (var i = 0; i < scope.categories.length; i += 1) {
						if (scope.categories[i] === scope.selectedPoint) {
							page = Math.floor(i / (scope.pageSize ? scope.pageSize : 10));
							Build();
							break;
						}
					}
				}
			}
			
			function OnSelectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = event.target.category;
					}, 100);
				}
				scope.onSelect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.category
					}
				});
			}
			
			function OnUnselectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = null;
					}, 0);
				}
				scope.onUnselect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.category
					}
				});
			}
		}
	}
	
})();