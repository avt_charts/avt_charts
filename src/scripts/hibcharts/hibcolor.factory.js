(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.factory('hibColor', hibColorFactory);
	
	hibColorFactory.$inject = ['_', 'HibchartsOptions'];
	
	function hibColorFactory(_, HibchartsOptions) {
		
		var _colors = {};
		
		
		var factory = {
			setAlfa: setAlfa,
			formatterNumber: _formatterNumber,
			set: set,
			init: init
		};
		
		function _formatterNumber(aux) {
			return aux !== null ? aux.toString().replace(/(\.\d*)/, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '';
		}
		
		function isRegistered(filterName) {
			return _colors[filterName] !== undefined;
		}
		
		
		function init(filterName, colors) {
			// console.log("Inicializar colorMap " + filterName);
			_colors[filterName] = {
				colors: colors ? colors : HibchartsOptions.defaultColors,
				count: 0,
				keys: {}
			};
			// console.log(_colors[filterName]);
		}
		
		
		function set(filterName, value) {
			// console.log("Registrar en colorMap: " + filterName + " key " + value);
			if (isRegistered(filterName)) {
				var found = _colors[filterName].keys[value];
				
				if (found) {
					// console.log("Color ya registrado:" + _colors[filterName].keys[value]);
					return _colors[filterName].keys[value];
				} else {
					_colors[filterName].keys[value] = _colors[filterName].colors[_colors[filterName].count];
					_colors[filterName].count = _colors[filterName].count + 1;
					// console.log(_colors[filterName].keys);
					// console.log("Registrar Color: " + value + " " + _colors[filterName].keys[value]);
					return _colors[filterName].keys[value];
				}
			}
			else {
				// console.log('DSFILTERS: ' + filterName + ' is not registered');
			}
		}
		
		function setAlfa(color, alfa) {
			return color.replace(/[^,]+(?=\))/, alfa);
		}
		
		
		return factory;
	}
})();