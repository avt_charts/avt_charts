(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibDonut', HibDonutDirective);
	
	HibDonutDirective.$inject = ['$timeout', 'HibchartsOptions', 'hibColor'];
	
	function HibDonutDirective($timeout, HibchartsOptions, hibColor) {
		return {
			restrict: 'E',
			scope: {
				allowHover: '=',
				animationMs: '=',
				background: '@',
				categories: '=',
				colorFactory: '=',
				colors: '=',
				hbLegend: '=',
				noBorder: '=',
				noMargin: '=',
				legendFormatter: '=',
				onSelect: '&',
				onUnselect: '&',
				position: '=',
				semi: '=',
				selectable: '=',
				selectedOffset: '=',
				selectedPoint: '=',
				series: '=',
				symbolRadius: '=',
				tooltip: '=',
				maxHeightLegend: '='
			},
			link: HibDonutDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			'<div></div>' +
			'<div ng-if="loading" class="no-data no-data-donut"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function HibDonutDirectiveLink(scope, element) {
			
			var chartElement = element[0].lastChild.firstChild;
			var noDataElement = element[0].lastChild.lastChild;
			var chart = null;
			
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			scope.$watch('stack', function () {
				Build();
			});
			scope.$watch('selectedPoint', function () {
				SelectPoint();
			});
			
			///////
			
			function Build() {
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				var series = PrepareData(scope.series, scope.categories, scope.colorFactory, hibColor);
				
				scope.loading = scope.series["loading"];
				
				var seriesOptions = {
					animation: {duration: scope.animationMs ? scope.animationMs : 1000},
					colorByPoint: true,
					slicedOffset: scope.selectedOffset ? scope.selectedOffset : 0,
					point: {events: {select: OnSelectHandler, unselect: OnUnselectHandler}},
					states: {
						select: scope.semi ? HibchartsOptions.semidonutSelectedStyle : HibchartsOptions.donutSelectedStyle,
						hover: {enabled: scope.allowHover ? true : false}
					},
					borderWidth: scope.noBorder ? 0 : 1,
					cursor: scope.selectable ? 'pointer' : '',
					allowPointSelect: scope.selectable ? true : false
				};
				
				var pieOptions = {
					shadow: false,
					startAngle: scope.semi ? -90 : 0,
					endAngle: scope.semi ? 90 : 360,
					center: ['50%', scope.semi ? '75%' : '50%'],
					dataLabels: scope.semi ? HibchartsOptions.semidonutDataLabels : HibchartsOptions.donutDataLabels
				};
				
				var chartOptions = {
					series: series,
					lang: {
						noData: "No Data Display"
					},
					title: {text: ''},
					colors: scope.colors ? scope.colors : HibchartsOptions.defaultColors,
					chart: {
						type: 'pie',
						backgroundColor: scope.background ? scope.background : 'transparent',
						marginTop: scope.noMargin ? null : 50,
						marginLeft: scope.noMargin ? null : 70,
						marginRight: scope.noMargin ? null : scope.position === 'lateral' ? 100 : 50,
						marginBottom: scope.noMargin ? null : scope.position === 'lateral' ? 50 : 100
					},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {
						enabled: scope.hbLegend ? true : false,
						layout: scope.position === 'lateral' ? 'vertical' : 'horizontal',
						verticalAlign: scope.position === 'lateral' ? 'middle' : 'bottom',
						align: scope.position === 'lateral' ? 'right' : 'center',
						maxHeight: scope.maxHeightLegend ? scope.maxHeightLegend : 80,
						symbolHeight: scope.symbolRadius ? 6 : null,
						symbolWidth: scope.symbolRadius ? 6 : null,
						symbolRadius: scope.symbolRadius ? 6 : null,
						useHTML: typeof scope.legendFormatter !== 'undefined' ? true : false,
						labelFormatter: typeof scope.legendFormatter !== 'undefined' ? scope.legendFormatter : function () {
							return this.name
						},
						itemStyle: HibchartsOptions.legendItemStyle,
						navigation: {
							activeColor: '#3E576F',
							animation: true,
							arrowSize: 12,
							inactiveColor: '#CCC',
							style: {
								fontWeight: 'bold',
								color: '#333',
								fontSize: '12px'
							}
						}
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: seriesOptions,
						pie: pieOptions
					}
				};
				
				chart = Highcharts.chart(chartElement, chartOptions);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				SelectPoint();
			}
			
			function SelectPoint() {
			}
			
			function OnSelectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = event.target.name;
					}, 100);
				}
				scope.onSelect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.name
					}
				});
			}
			
			function OnUnselectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = null;
					}, 0);
				}
				scope.onUnselect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.name
					}
				});
			}
		}
	}
	
	function PrepareData(rawSeries, categories, colorFactory, hibColor) {
		var hasData = rawSeries && rawSeries.length && rawSeries.length > 0;
		var series = [];
		
		if (hasData) {
			rawSeries = _.orderBy(rawSeries, 'group', 'desc');
			var sizeDecrement = 70 / rawSeries.length;
			var size = 100, innerSize = 100 - sizeDecrement;
			for (var i = 0; i < rawSeries.length; i += 1) {
				var serie = {
					name: rawSeries[i].name,
					data: [],
					size: size + '%',
					innerSize: (innerSize < 40 ? 40 : innerSize) + '%',
					showInLegend: i === 0,
				};
				
				for (var j = 0; j < rawSeries[i].data.length; j += 1) {
					var obj = {
						name: categories[j],
						y: rawSeries[i].data[j],
						events: {
							legendItemClick: function () {
								return false;
							}
						}
					};
					if (colorFactory) obj.color = hibColor.set(colorFactory, obj['name']);
					serie.data.push(obj);
				}
				
				size -= sizeDecrement;
				innerSize = size - sizeDecrement;
				series.push(serie);
			}
		}
		
		return series;
	}
	
})();