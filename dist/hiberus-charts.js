(function() { 

  'use strict';

  angular.module('hiberus-charts', [ 
    'angular-svg-round-progress'
    ]);

})();
(function () {
	'use strict';
	angular.module('hiberus-charts').directive('hibProgressBar', HibProgressBarDirective);
	
	HibProgressBarDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	function HibProgressBarDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			link: HibProgressBarDirectiveLink,
			templateUrl: 'html/doubleprogressbar.template.html',
			scope: {
				series: '=',
				colors: '=',
				prevColumn: '@',
				currentColumn: '@',
				nameCurrentColumn: '@',
				namePrevColumn: '@',
				animationDuration: '@'
			}
		};
	}
	
	
	function HibProgressBarDirectiveLink(scope, element) {
		var chartElement = element[0].firstElementChild.lastElementChild;
		var noDataElement = element[0].lastChild;
		
		var chart = null;
		
		
		scope.$watch('series', function () {
			Build();
		});
		
		function Build() {
			
			var dataSeries = [];
			
			var hasData = scope.series;
			
			var colors = scope.colors ? scope.colors : ['#45CCCE', '#004d45'];
			
			var prevColumn = scope.prevColumn ? scope.prevColumn : 'spend';
			var currentColumn = scope.currentColumn ? scope.currentColumn : 'budget';
			var namePrevColumn = scope.namePrevColumn ? scope.namePrevColumn : 'Total Spend';
			var nameCurrentColumn = scope.nameCurrentColumn ? scope.nameCurrentColumn : 'Total Budget';
			
			if (scope.series) {
				scope.valueRoundProgress = ((scope.series[currentColumn] - scope.series[prevColumn]) / scope.series[currentColumn]) * 100;
				
				var obj = {
					name: namePrevColumn,
					data: [scope.series[prevColumn]],
					pointWidth: 6,
					series: {
						pointPadding: 15,
						groupPadding: 10
					},
					borderWidth: 0,
					animation: {
						duration: scope.animationDuration !== undefined ? parseInt(scope.animationDuration) : 3000,
					}
				};
				dataSeries.push(obj);
				var obj = {
					name: nameCurrentColumn,
					data: [scope.series[currentColumn]],
					pointWidth: 6,
					series: {
						pointPadding: 15,
						groupPadding: 10
					},
					borderWidth: 0,
					animation: {
						duration: scope.animationDuration !== undefined ? parseInt(scope.animationDuration) : 3000
					}
				};
				dataSeries.push(obj);
				
			}
			
			var chartOptions = {
				chart: {
					type: 'bar',
					margin: [80, 10, 55, 0],
					backgroundColor: null,
					events: {
						load: function () {
							addRoundProgress();
						}
					}
				},
				colors: colors,
				tooltip: {
					enabled: false
				},
				title: {
					text: null
				},
				legend: {
					symbolWidth: 3,
					symbolHeight: 3,
					symbolRadius: 5,
					y: 0,
					enabled: true,
					floating: false,
					verticalAlign: 'top',
					align: 'left',
					layout: 'horizontal',
					useHTML: true,
					labelFormatter: function () {
						return '<div><span style="float:left;clear:both;display:block;font-size:.8rem!important;font-family:Gotham-Light !important;text-transform: uppercase;">' + this.name + '</span><span style="float:left;clear:both;display:blockfont-size:12px;font-family:Gotham !important">' + this.yData[0] + '</span></div>';
					}
				},
				exporting: {enabled: false},
				credits: {
					enabled: false
				},
				xAxis: {
					lineWidth: 1
				},
				yAxis: {
					gridLineWidth: 1,
					lineWidth: 0,
					min: 0,
					title: {
						enabled: false
					},
					labels: {
						enabled: true
					}
				},
				series: dataSeries
			};
			
			
			chart = Highcharts.chart(chartElement, chartOptions);
			$(noDataElement).css('display', hasData ? 'none' : 'block');
			
			
			scope.showChart = true;
			scope.isLoading = false;
		}
		
		function addRoundProgress() {
			scope.showRoundProgress = true;
		}
	}
})();
(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibBubbles', HibBubblesDirective);
	
	HibBubblesDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	
	function HibBubblesDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			scope: {
				background: '@',
				hAxisTitle: '@',
				maxX: '=',
				maxY: '=',
				minX: '=',
				minY: '=',
				series: '=',
				labels: '=',
				sizeBubleMin: '=',
				sizeBubleMax: '=',
				tooltip: '=',
				vAxisTitle: '@',
				axisColorX: '=',
				axisColorY: '=',
				gridColorX: '=',
				gridColorY: '=',
				tickLength: '=',
				tickColor: '='
			},
			link: HibBubblesDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			'<div></div>' +
			'<div class="no-data no-data-bubble"><div class="spnldng"><span class="sfr_span">NO DATA DISPLAY</span><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function HibBubblesDirectiveLink(scope, element) {
			
			var chartElement = element[0].lastChild.firstChild;
			var noDataElement = element[0].lastChild.lastChild;
			var chart = null;
			var ren, line, area1, area2;
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			
			///////
			
			function Build() {
				console.log(scope)
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				var series = JSON.parse(JSON.stringify(scope.series));
				//series.push({ name: '', data: [{ x: 0, y: 0, z: 0 }], showInLegend: false, color: 'transparent', enableMouseTracking: false });
				
				var options = {
					chart: {
						type: 'bubble',
						plotBorderWidth: 0,
						backgroundColor: scope.background ? scope.background : 'transparent',
						zoomType: 'xy',
						events: {
							load: function () {
								drawSVG(this, false);
							},
							redraw: function () {
								drawSVG(this, true);
							}
						}
					},
					title: {text: ''},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {enabled: false},
					xAxis: {
						lineWidth: 1,
						gridLineWidth: 1,
						max: scope.maxX ? scope.maxX : null,
						min: 0,
						title: {text: scope.hAxisTitle},
						labels: {
							enabled: true,
							format: scope.labels ? scope.labels : "{value}%"
						},
						lineColor: scope.axisColorX ? scope.axisColorX : '#ccd6eb',
						gridLineColor: scope.gridColorX ? scope.gridColorX : '#e6e6e6',
						tickLength: scope.tickLength ? scope.tickLength : 10,
						tickColor: scope.tickColor ? scope.tickColor : '#ccd6eb'
					},
					yAxis: {
						lineWidth: 1,
						gridLineWidth: 1,
						max: scope.maxY ? scope.maxY : null,
						min: 0,
						title: {text: scope.vAxisTitle},
						labels: {
							enabled: true,
							format: scope.labels ? scope.labels : "{value}%"
						},
						lineColor: scope.axisColorY ? scope.axisColorY : '#ccd6eb',
						gridLineColor: scope.gridColorY ? scope.gridColorY : '#e6e6e6'
						
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: {
							dataLabels: {
								enabled: true,
								formatter: function () {
									return this.key
								},
								style: {
									"color": "#ffffff",
									"fontWeight": "100",
									"fontSize": "10px",
									"textShadow": "0 0 6px rgba(0, 0, 0, 0.75), 0 0 3px rgba(0, 0, 0, 0.75)"
								}
							}
						},
						bubble: {
							minSize: scope.sizeBubleMin ? scope.sizeBubleMin : '20',
							maxSize: scope.sizeBubleMax ? scope.sizeBubleMax : '20',
							marker: {lineWidth: 0, fillOpacity: 1, states: {hover: {enabled: false}}}
						}
					},
					series: series
				};
				
				chart = Highcharts.chart(chartElement, options);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				
				if (!hasData) {
					line.destroy();
					area1.destroy();
					area2.destroy();
					if (!scope.series) {
						$(noDataElement).find('.sfr').css('display', 'none');
						$(noDataElement).find('.sfr_span').css('display', 'block');
					}
				}
			}
			
			function drawSVG(chartElement, destroy) {
				if (destroy) {
					line.destroy();
					area1.destroy();
					area2.destroy();
				}
				ren = chartElement.renderer;
				line = ren.path(['M', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop])
				.attr({
					'stroke-width': 1,
					stroke: 'rgba(128, 128, 128, 0.35)',
					dashstyle: 'dash'
				})
				.add();
				area1 = ren.path(['M', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop, 'L'
					, chartElement.plotLeft, chartElement.plotTop, 'L', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight])
				.attr({
					'stroke-width': 0,
					stroke: 'silver',
					dashstyle: 'dash',
					fill: "rgba(0, 128, 0, 0.3)",
					'fill-opacity': ".2"
				})
				.add();
				area2 = ren.path(['M', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop, 'L'
					, chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight])
				.attr({
					'stroke-width': 0,
					stroke: 'silver',
					dashstyle: 'dash',
					fill: "rgba(255, 0, 0, 0.38)",
					'fill-opacity': ".2"
				})
				.add();
			}
			
		}
	}
	
})();
(function () {

  'use strict';

  angular
    .module('hiberus-charts')
    .factory('HibchartsOptions', HibchartsOptionsFactory);

  function HibchartsOptionsFactory() {

    var columnsSelectedStyle = {
      color: '#fefefe',
      borderColor: 'rgb(124, 181, 236)'
    };

    var donutSelectedStyle = {
      color: 'black',
      borderColor: 'white'
    };

    var semidonutSelectedStyle = {

    };

    var legendItemStyle = {
    };

    var donutDataLabels = {
      color: 'white',
      distance: -25,
      formatter: function () { return Highcharts.numberFormat(this.percentage,1) + '%'; },
      style: {
        textShadow: 'none',
        fontWeight: 'bold'
      }
    };

    var semidonutDataLabels = {
      color: 'white',
      distance: -25,
      formatter: function () { return Highcharts.numberFormat(this.percentage,1) + '%'; },
      style: {
        textShadow: 'none',
        fontWeight: 'bold'
      }
    };

       var PointFormatter = function() {
         var text = Highcharts.numberFormat(this.y, 0);

      if (this.percentage !== undefined) {
        text = Highcharts.numberFormat(this.percentage,1) + '%';
      }

      if (this.z !== undefined) {
        text = '(' + this.x + ', ' + this.y + ') ';
      }

      return '<b>' + text + '</b>';
    }

    var defaultTooltip = {
      enabled: true,
      backgroundColor: '#000',
      borderColor: 'black',
      borderRadius: 10,
      borderWidth: 3,
      style: { color: '#fff', fontWeight: 'bold', fontSize: '12px', fontFamily: 'inherit' },
      pointFormatter: PointFormatter
    };
    
    

    var defaultColors = ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1'];

    return {
      columnsSelectedStyle: columnsSelectedStyle,
      donutSelectedStyle: donutSelectedStyle,
      donutDataLabels: donutDataLabels,
      semidonutSelectedStyle: semidonutSelectedStyle,
      semidonutDataLabels: semidonutDataLabels,
      defaultColors: defaultColors,
      defaultTooltip: defaultTooltip,
      legendItemStyle: legendItemStyle
    };


  }

})();
(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.factory('hibColor', hibColorFactory);
	
	hibColorFactory.$inject = ['_', 'HibchartsOptions'];
	
	function hibColorFactory(_, HibchartsOptions) {
		
		var _colors = {};
		
		
		var factory = {
			setAlfa: setAlfa,
			formatterNumber: _formatterNumber,
			set: set,
			init: init
		};
		
		function _formatterNumber(aux) {
			return aux !== null ? aux.toString().replace(/(\.\d*)/, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '';
		}
		
		function isRegistered(filterName) {
			return _colors[filterName] !== undefined;
		}
		
		
		function init(filterName, colors) {
			// console.log("Inicializar colorMap " + filterName);
			_colors[filterName] = {
				colors: colors ? colors : HibchartsOptions.defaultColors,
				count: 0,
				keys: {}
			};
			// console.log(_colors[filterName]);
		}
		
		
		function set(filterName, value) {
			// console.log("Registrar en colorMap: " + filterName + " key " + value);
			if (isRegistered(filterName)) {
				var found = _colors[filterName].keys[value];
				
				if (found) {
					// console.log("Color ya registrado:" + _colors[filterName].keys[value]);
					return _colors[filterName].keys[value];
				} else {
					_colors[filterName].keys[value] = _colors[filterName].colors[_colors[filterName].count];
					_colors[filterName].count = _colors[filterName].count + 1;
					// console.log(_colors[filterName].keys);
					// console.log("Registrar Color: " + value + " " + _colors[filterName].keys[value]);
					return _colors[filterName].keys[value];
				}
			}
			else {
				// console.log('DSFILTERS: ' + filterName + ' is not registered');
			}
		}
		
		function setAlfa(color, alfa) {
			return color.replace(/[^,]+(?=\))/, alfa);
		}
		
		
		return factory;
	}
})();
(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibColumns', HibColumnsDirective);
	
	HibColumnsDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	function HibColumnsDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			scope: {
				animationMs: '=',
				background: '@',
				categories: '=',
				colors: '=',
				colorByPoint: '=',
				labelsDataSeries: '=',
				heightAuto: '=',
				isRoundProgress: '=',
				hbLegend: '=',
				labelFormatter: '=',
				noBorder: '=',
				noMargin: '=',
				marker: '=',
				onSelect: '&',
				onUnselect: '&',
				pageSize: '=',
				paginable: '=',
				position: '@',
				rotatedLabels: '=',
				selectable: '=',
				selectedPoint: '=',
				series: '=',
				stack: '=',
				symbolRadius: '=',
				tooltip: '=',
				type: '=',
				axisXLineWidth: '=',
				axisXTickWidth: '=',
				yAxisLabelsHidden: '=',
				yAxisTitle: '@',
				yAxisMax: '=',
				yAxisLineWidth: '=',
				yAxisGridLineWidth: '=',
				zoomType: '=',
				vAxisTitle: '@',
				axisColorX: '=',
				axisColorY: '=',
				gridColorX: '=',
				gridColorY: '=',
				tickLength: '=',
				tickColor: '='
			},
			link: HibColumnsDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			'<hb-round-progress-column ng-if="initRoundProgress" length="length"  values-round-progress="valuesRoundProgress"></hb-round-progress-column>' +
			'<div></div>' +
			'<div class="button-wrapper type-arrow" ng-if="paginable">' +
			'<a href="" ng-click="PageLeft()"><i class="brand-icon brand-icon-arrow prev" ng-class="{ inactive : !canPageLeft }"></i></a>' +
			'<a href="" ng-click="PageRight()"><i class="brand-icon brand-icon-arrow next" ng-class="{ inactive : !canPageRight }"></i></a>' +
			'</div>' +
			'<div ng-if="loading" class="no-data no-data-columns"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function HibColumnsDirectiveLink(scope, element) {
			var chartElement = element[0].lastChild.firstChild.nextSibling;
			var noDataElement = element[0].lastChild.lastChild;
			var page = 0;
			var chart = null;
			
			scope.canPageLeft = false;
			scope.canPageRight = false;
			scope.PageRight = function () {
				if (scope.canPageRight) {
					page += 1;
					Build();
				}
			};
			scope.PageLeft = function () {
				if (scope.canPageLeft) {
					page -= 1;
					Build();
				}
			};
			
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			scope.$watch('stack', function () {
				Build();
			});
			scope.$watch('selectedPoint', function () {
				if (!SelectPoint()) {
					PaginateToSelected();
				}
			});
			
			///////
			
			function Build() {
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				scope.loading = scope.series["loading"];
				
				var series = JSON.parse(JSON.stringify(scope.series));
				var categories = JSON.parse(JSON.stringify(scope.categories));
				scope.initRoundProgress = false;
				scope.valuesRoundProgress = [];
				
				if (scope.paginable) {
					var pageSize = scope.pageSize ? scope.pageSize : 10;
					var start = page * pageSize;
					var end = start + pageSize;
					var dataLength = hasData ? series[0].data.length : 0;
					
					scope.canPageLeft = page > 0;
					scope.canPageRight = end < dataLength;
					
					for (var i = 0; i < series.length; i += 1) {
						series[i].data = series[i].data.slice(start, end);
					}
					categories = categories.slice(start, end);
				}
				
				scope.length = categories.length;
				
				if (scope.isRoundProgress && series[1] != null) {
					scope.initRoundProgress = true;
					_.forEach(categories, function (value, index) {
						scope.valuesRoundProgress.push({
							value: series[1].data[index] != null && series[0].data[index] > 0 && series[1].data[index] > 0 ? ((series[1].data[index] - series[0].data[index]) / series[0].data[index]) * 100 : null,
							color: series[1].data[index] - series[0].data[index] > 0 ? '#0088ec' : 'red'
						});
					});
					
				}
				
				
				var seriesOptions = {
					animation: {duration: scope.animationMs ? scope.animationMs : 1000},
					colorByPoint: scope.colorByPoint ? true : false,
					dataLabels: {
						enabled: scope.labelsDataSeries ? true : false,
					},
					point: {
						events: {
							select: OnSelectHandler,
							unselect: OnUnselectHandler
						}
					},
					borderWidth: scope.noBorder ? 0 : 1,
					states: {select: HibchartsOptions.columnsSelectedStyle},
					cursor: scope.selectable ? 'pointer' : '',
					allowPointSelect: scope.selectable ? true : false,
					marker: {
						enabled: scope.marker ? true : false
					}
				};
				
				var options = {
					series: series,
					lang: {
						noData: "No Data Display"
					},
					xAxis: {
						categories: categories,
						lineWidth: !scope.axisXLineWidth && typeof scope.axisXLineWidth !== 'undefined' ? scope.axisXLineWidth : 1,
						tickWidth: !scope.axisXTickWidth && typeof scope.axisXTickWidth !== 'undefined' ? scope.axisXTickWidth : 1,
						labels: {
							rotation: scope.rotatedLabels ? -45 : 0,
							useHTML: true,
							formatter: scope.labelFormatter ? scope.labelFormatter : undefined
						},
						lineColor: scope.axisColorX ? scope.axisColorX : '#ccd6eb',
						gridLineColor: scope.gridColorX ? scope.gridColorX : '#e6e6e6',
						tickLength: scope.tickLength ? scope.tickLength : 10,
						tickColor: scope.tickColor ? scope.tickColor : '#ccd6eb'
					},
					yAxis: {
						title: {text: scope.yAxisTitle},
						max: !scope.yAxisMax ? null : scope.yAxisMax,
						lineWidth: !scope.yAxisLineWidth && typeof scope.yAxisLineWidth !== 'undefined' ? scope.yAxisLineWidth : 1,
						gridLineWidth: !scope.yAxisGridLineWidth && typeof scope.yAxisGridLineWidth !== 'undefined' ? scope.yAxisGridLineWidth : 1,
						labels: {
							enabled: scope.yAxisLabelsHidden ? false : true,
						},
						lineColor: scope.axisColorY ? scope.axisColorY : '#ccd6eb',
						gridLineColor: scope.gridColorY ? scope.gridColorY : '#e6e6e6'
					},
					title: {text: ''},
					colors: scope.colors ? scope.colors : HibchartsOptions.defaultColors,
					chart: {
						type: scope.type ? scope.type : 'column',
						backgroundColor: scope.background ? scope.background : 'transparent',
						marginTop: scope.noMargin ? null : scope.isRoundProgress ? 20 : 50,
						marginLeft: scope.noMargin && !scope.paginable ? null : scope.paginable && scope.noMargin ? 70 : scope.paginable ? 85 : 70,
						marginRight: scope.noMargin && !scope.paginable ? null : scope.position === 'lateral' ? 100 : scope.noMargin && scope.paginable ? 25 : 50,
						marginBottom: scope.noMargin ? null : scope.position === 'lateral' ? 50 : 100,
						height: scope.heightAuto ? null : scope.isRoundProgress && scope.initRoundProgress ? 350 : 400,
						zoomType: scope.zoomType ? scope.zoomType : null,
					},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {
						enabled: scope.hbLegend ? true : false,
						layout: scope.position === 'lateral' ? 'vertical' : 'horizontal',
						verticalAlign: scope.position === 'lateral' ? 'middle' : 'bottom',
						align: scope.position === 'lateral' ? 'right' : 'center',
						itemStyle: HibchartsOptions.legendItemStyle,
						maxHeight: 80,
						symbolHeight: scope.symbolRadius ? 6 : null,
						symbolWidth: scope.symbolRadius ? 6 : null,
						symbolRadius: scope.symbolRadius ? 6 : null,
						navigation: {
							activeColor: '#3E576F',
							animation: true,
							arrowSize: 12,
							inactiveColor: '#CCC',
							style: {
								fontWeight: 'bold',
								color: '#333',
								fontSize: '12px'
							}
						}
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: seriesOptions,
						column: {stacking: scope.stack ? 'normal' : false}
					}
				};
				
				
				chart = Highcharts.chart(chartElement, options);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				SelectPoint();
			}
			
			function SelectPoint() {
				if (scope.selectedPoint !== undefined && chart !== null && chart.series.length > 0) {
					for (var i = 0; i < chart.series[0].data.length; i += 1) {
						if (chart.series[0].data[i].category === scope.selectedPoint) {
							chart.series[0].data[i].select(true, false);
							return true;
						}
						else if (scope.selectedPoint === null) {
							if (chart.series[0].data[i].selected) {
								chart.series[0].data[i].select(false);
							}
						}
					}
				}
				
				return false;
			}
			
			function PaginateToSelected() {
				if (scope.paginable && scope.selectedPoint !== undefined) {
					for (var i = 0; i < scope.categories.length; i += 1) {
						if (scope.categories[i] === scope.selectedPoint) {
							page = Math.floor(i / (scope.pageSize ? scope.pageSize : 10));
							Build();
							break;
						}
					}
				}
			}
			
			function OnSelectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = event.target.category;
					}, 100);
				}
				scope.onSelect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.category
					}
				});
			}
			
			function OnUnselectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = null;
					}, 0);
				}
				scope.onUnselect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.category
					}
				});
			}
		}
	}
	
})();
(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibDonut', HibDonutDirective);
	
	HibDonutDirective.$inject = ['$timeout', 'HibchartsOptions', 'hibColor'];
	
	function HibDonutDirective($timeout, HibchartsOptions, hibColor) {
		return {
			restrict: 'E',
			scope: {
				allowHover: '=',
				animationMs: '=',
				background: '@',
				categories: '=',
				colorFactory: '=',
				colors: '=',
				hbLegend: '=',
				noBorder: '=',
				noMargin: '=',
				legendFormatter: '=',
				onSelect: '&',
				onUnselect: '&',
				position: '=',
				semi: '=',
				selectable: '=',
				selectedOffset: '=',
				selectedPoint: '=',
				series: '=',
				symbolRadius: '=',
				tooltip: '=',
				maxHeightLegend: '='
			},
			link: HibDonutDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			'<div></div>' +
			'<div ng-if="loading" class="no-data no-data-donut"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function HibDonutDirectiveLink(scope, element) {
			
			var chartElement = element[0].lastChild.firstChild;
			var noDataElement = element[0].lastChild.lastChild;
			var chart = null;
			
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			scope.$watch('stack', function () {
				Build();
			});
			scope.$watch('selectedPoint', function () {
				SelectPoint();
			});
			
			///////
			
			function Build() {
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				var series = PrepareData(scope.series, scope.categories, scope.colorFactory, hibColor);
				
				scope.loading = scope.series["loading"];
				
				var seriesOptions = {
					animation: {duration: scope.animationMs ? scope.animationMs : 1000},
					colorByPoint: true,
					slicedOffset: scope.selectedOffset ? scope.selectedOffset : 0,
					point: {events: {select: OnSelectHandler, unselect: OnUnselectHandler}},
					states: {
						select: scope.semi ? HibchartsOptions.semidonutSelectedStyle : HibchartsOptions.donutSelectedStyle,
						hover: {enabled: scope.allowHover ? true : false}
					},
					borderWidth: scope.noBorder ? 0 : 1,
					cursor: scope.selectable ? 'pointer' : '',
					allowPointSelect: scope.selectable ? true : false
				};
				
				var pieOptions = {
					shadow: false,
					startAngle: scope.semi ? -90 : 0,
					endAngle: scope.semi ? 90 : 360,
					center: ['50%', scope.semi ? '75%' : '50%'],
					dataLabels: scope.semi ? HibchartsOptions.semidonutDataLabels : HibchartsOptions.donutDataLabels
				};
				
				var chartOptions = {
					series: series,
					lang: {
						noData: "No Data Display"
					},
					title: {text: ''},
					colors: scope.colors ? scope.colors : HibchartsOptions.defaultColors,
					chart: {
						type: 'pie',
						backgroundColor: scope.background ? scope.background : 'transparent',
						marginTop: scope.noMargin ? null : 50,
						marginLeft: scope.noMargin ? null : 70,
						marginRight: scope.noMargin ? null : scope.position === 'lateral' ? 100 : 50,
						marginBottom: scope.noMargin ? null : scope.position === 'lateral' ? 50 : 100
					},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {
						enabled: scope.hbLegend ? true : false,
						layout: scope.position === 'lateral' ? 'vertical' : 'horizontal',
						verticalAlign: scope.position === 'lateral' ? 'middle' : 'bottom',
						align: scope.position === 'lateral' ? 'right' : 'center',
						maxHeight: scope.maxHeightLegend ? scope.maxHeightLegend : 80,
						symbolHeight: scope.symbolRadius ? 6 : null,
						symbolWidth: scope.symbolRadius ? 6 : null,
						symbolRadius: scope.symbolRadius ? 6 : null,
						useHTML: typeof scope.legendFormatter !== 'undefined' ? true : false,
						labelFormatter: typeof scope.legendFormatter !== 'undefined' ? scope.legendFormatter : function () {
							return this.name
						},
						itemStyle: HibchartsOptions.legendItemStyle,
						navigation: {
							activeColor: '#3E576F',
							animation: true,
							arrowSize: 12,
							inactiveColor: '#CCC',
							style: {
								fontWeight: 'bold',
								color: '#333',
								fontSize: '12px'
							}
						}
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: seriesOptions,
						pie: pieOptions
					}
				};
				
				chart = Highcharts.chart(chartElement, chartOptions);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				SelectPoint();
			}
			
			function SelectPoint() {
			}
			
			function OnSelectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = event.target.name;
					}, 100);
				}
				scope.onSelect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.name
					}
				});
			}
			
			function OnUnselectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = null;
					}, 0);
				}
				scope.onUnselect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.name
					}
				});
			}
		}
	}
	
	function PrepareData(rawSeries, categories, colorFactory, hibColor) {
		var hasData = rawSeries && rawSeries.length && rawSeries.length > 0;
		var series = [];
		
		if (hasData) {
			rawSeries = _.orderBy(rawSeries, 'group', 'desc');
			var sizeDecrement = 70 / rawSeries.length;
			var size = 100, innerSize = 100 - sizeDecrement;
			for (var i = 0; i < rawSeries.length; i += 1) {
				var serie = {
					name: rawSeries[i].name,
					data: [],
					size: size + '%',
					innerSize: (innerSize < 40 ? 40 : innerSize) + '%',
					showInLegend: i === 0,
				};
				
				for (var j = 0; j < rawSeries[i].data.length; j += 1) {
					var obj = {
						name: categories[j],
						y: rawSeries[i].data[j],
						events: {
							legendItemClick: function () {
								return false;
							}
						}
					};
					if (colorFactory) obj.color = hibColor.set(colorFactory, obj['name']);
					serie.data.push(obj);
				}
				
				size -= sizeDecrement;
				innerSize = size - sizeDecrement;
				series.push(serie);
			}
		}
		
		return series;
	}
	
})();
(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibEqualizer', hibEqualizerDirective);
	
	hibEqualizerDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	function hibEqualizerDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			scope: {
				animationMs: '=',
				background: '@',
				categories: '=',
				colors: '=',
				colorByPoint: '=',
				labelsDataSeries: '=',
				heightAuto: '=',
				// isRoundProgress: '=',
				hbLegend: '=',
				labelFormatter: '=',
				hbBorder: '=',
				noMargin: '=',
				marker: '=',
				onSelect: '&',
				onUnselect: '&',
				pageSize: '=',
				paginable: '=',
				position: '@',
				rotatedLabels: '=',
				selectable: '=',
				selectedPoint: '=',
				series: '=',
				stack: '=',
				symbolRadius: '=',
				tooltip: '=',
				// type: '=',
				// axisXLineWidth: '=',
				// axisXTickWidth: '=',
				yAxisLabelsHidden: '=',
				yAxisTitle: '@',
				yAxisMax: '=',
				yAxisLineWidth: '=',
				yAxisGridLineWidth: '=',
				zoomType: '=',
				axisColorX: '=',
				axisColorY: '=',
				gridColorX:'=',
				gridColorY: '='
			},
			link: hibEqualizerDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			// '<hb-round-progress-column ng-if="initRoundProgress" length="length"  values-round-progress="valuesRoundProgress"></hb-round-progress-column>' +
			'<div></div>' +
			'<div class="button-wrapper type-arrow" ng-if="paginable">' +
			'<a href="" ng-click="PageLeft()"><i class="brand-icon brand-icon-arrow prev" ng-class="{ inactive : !canPageLeft }"></i></a>' +
			'<a href="" ng-click="PageRight()"><i class="brand-icon brand-icon-arrow next" ng-class="{ inactive : !canPageRight }"></i></a>' +
			'</div>' +
			'<div ng-if="loading" class="no-data no-data-columns"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function hibEqualizerDirectiveLink(scope, element) {
			var chartElement = element[0].lastChild.firstChild;
			var noDataElement = element[0].lastChild.lastChild;
			var page = 0;
			var chart = null;
			
			scope.canPageLeft = false;
			scope.canPageRight = false;
			scope.PageRight = function () {
				if (scope.canPageRight) {
					page += 1;
					Build();
				}
			};
			scope.PageLeft = function () {
				if (scope.canPageLeft) {
					page -= 1;
					Build();
				}
			};
			
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			scope.$watch('stack', function () {
				Build();
			});
			scope.$watch('selectedPoint', function () {
				if (!SelectPoint()) {
					PaginateToSelected();
				}
			});
			
			///////
			
			function Build() {
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				scope.loading = scope.series["loading"];
				
				var series = JSON.parse(JSON.stringify(scope.series));
				var categories = JSON.parse(JSON.stringify(scope.categories));
				// scope.initRoundProgress = false;
				// scope.valuesRoundProgress = [];
				
				if (scope.paginable) {
					var pageSize = scope.pageSize ? scope.pageSize : 10;
					var start = page * pageSize;
					var end = start + pageSize;
					var dataLength = hasData ? series[0].data.length : 0;
					
					scope.canPageLeft = page > 0;
					scope.canPageRight = end < dataLength;
					
					for (var i = 0; i < series.length; i += 1) {
						series[i].data = series[i].data.slice(start, end);
					}
					categories = categories.slice(start, end);
				}
				
				scope.length = categories.length;
				
				// if (scope.isRoundProgress && series[1] != null) {
				// 	scope.initRoundProgress = true;
				// 	_.forEach(categories, function (value, index) {
				// 		scope.valuesRoundProgress.push({
				// 			value: series[1].data[index] != null && series[0].data[index] > 0 && series[1].data[index] > 0 ? ((series[1].data[index] - series[0].data[index]) / series[0].data[index]) * 100 : null,
				// 			color: series[1].data[index] - series[0].data[index] > 0 ? '#0088ec' : 'red'
				// 		});
				// 	});
				//
				// }
				
				
				var newData = [];
				if (hasData) {
					
					var data = series[0].data;
					
					
					newData.push({
						data: data
					});
					
					data.forEach(function (point, index) {
						var howMany = point / 5,
							i = 0;
						
						for (; i < howMany; i++) {
							newData.push({
								stack: 0,
								data: [{
									x: index,
									y: 5,
									color: 'transparent'
								}]
							});
						}
					});
					
				}
				
				var seriesOptions = {
					animation: {duration: scope.animationMs ? scope.animationMs : 1000},
					colorByPoint: scope.colorByPoint ? true : false,
					dataLabels: {
						enabled: scope.labelsDataSeries ? true : false,
					},
					point: {
						events: {
							select: OnSelectHandler,
							unselect: OnUnselectHandler
						}
					},
					states: {select: HibchartsOptions.columnsSelectedStyle},
					cursor: scope.selectable ? 'pointer' : '',
					allowPointSelect: scope.selectable ? true : false,
					marker: {
						enabled: scope.marker ? true : false
					},
					stacking: 'normal',
					grouping: false
					
				};
				
				var options = {
					series: newData,
					lang: {
						noData: "No Data Display"
					},
					xAxis: {
						categories: categories,
						tickWidth: 0,
						labels: {
							rotation: scope.rotatedLabels ? -45 : 0,
							useHTML: true,
							formatter: scope.labelFormatter ? scope.labelFormatter : undefined
						},
						lineColor: scope.axisColorX ? scope.axisColorX : '#ccd6eb',
						gridLineColor: scope.gridColorX ? scope.gridColorX : '#e6e6e6'
					},
					yAxis: {
						title: {text: scope.yAxisTitle},
						max: !scope.yAxisMax ? null : scope.yAxisMax,
						lineWidth: !scope.yAxisLineWidth && typeof scope.yAxisLineWidth !== 'undefined' ? scope.yAxisLineWidth : 1,
						gridLineWidth: !scope.yAxisGridLineWidth && typeof scope.yAxisGridLineWidth !== 'undefined' ? scope.yAxisGridLineWidth : 1,
						labels: {
							enabled: scope.yAxisLabelsHidden ? false : true,
						},
						lineColor: scope.axisColorY ? scope.axisColorY : '#ccd6eb',
						gridLineColor: scope.gridColorY ? scope.gridColorY : '#e6e6e6'
					},
					title: {text: ''},
					colors: scope.colors ? scope.colors : HibchartsOptions.defaultColors,
					chart: {
						type: 'column',
						backgroundColor: scope.background ? scope.background : 'transparent',
						marginTop: scope.noMargin ? null : scope.isRoundProgress ? 20 : 50,
						marginLeft: scope.noMargin && !scope.paginable ? null : scope.paginable && scope.noMargin ? 70 : scope.paginable ? 85 : 70,
						marginRight: scope.noMargin && !scope.paginable ? null : scope.position === 'lateral' ? 100 : scope.noMargin && scope.paginable ? 25 : 50,
						marginBottom: scope.noMargin ? null : scope.position === 'lateral' ? 50 : 100,
						height: scope.heightAuto ? null : scope.isRoundProgress && scope.initRoundProgress ? 350 : 400,
						zoomType: scope.zoomType ? scope.zoomType : null,
					},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {
						enabled: scope.hbLegend ? true : false,
						layout: scope.position === 'lateral' ? 'vertical' : 'horizontal',
						verticalAlign: scope.position === 'lateral' ? 'middle' : 'bottom',
						align: scope.position === 'lateral' ? 'right' : 'center',
						itemStyle: HibchartsOptions.legendItemStyle,
						maxHeight: 80,
						symbolHeight: scope.symbolRadius ? 6 : null,
						symbolWidth: scope.symbolRadius ? 6 : null,
						symbolRadius: scope.symbolRadius ? 6 : null,
						navigation: {
							activeColor: '#3E576F',
							animation: true,
							arrowSize: 12,
							inactiveColor: '#CCC',
							style: {
								fontWeight: 'bold',
								color: '#333',
								fontSize: '12px'
							}
						}
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: seriesOptions,
						column: {
							borderWidth: scope.hbBorder ? scope.hbBorder : 2.5,
							borderColor: 'rgba(0, 0, 0, 0.50)',
							borderRadius: 4
						}
					}
				};
				
				
				chart = Highcharts.chart(chartElement, options);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				SelectPoint();
			}
			
			function SelectPoint() {
				if (scope.selectedPoint !== undefined && chart !== null && chart.series.length > 0) {
					for (var i = 0; i < chart.series[0].data.length; i += 1) {
						if (chart.series[0].data[i].category === scope.selectedPoint) {
							chart.series[0].data[i].select(true, false);
							return true;
						}
						else if (scope.selectedPoint === null) {
							if (chart.series[0].data[i].selected) {
								chart.series[0].data[i].select(false);
							}
						}
					}
				}
				
				return false;
			}
			
			function PaginateToSelected() {
				if (scope.paginable && scope.selectedPoint !== undefined) {
					for (var i = 0; i < scope.categories.length; i += 1) {
						if (scope.categories[i] === scope.selectedPoint) {
							page = Math.floor(i / (scope.pageSize ? scope.pageSize : 10));
							Build();
							break;
						}
					}
				}
			}
			
			function OnSelectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = event.target.category;
					}, 100);
				}
				scope.onSelect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.category
					}
				});
			}
			
			function OnUnselectHandler(event) {
				if (scope.selectedPoint !== undefined) {
					$timeout(function () {
						scope.selectedPoint = null;
					}, 0);
				}
				scope.onUnselect({
					point: {
						value: event.target.y,
						name: event.target.series.name,
						category: event.target.category
					}
				});
			}
		}
	}
	
})();
(function() { 

  'use strict';

  angular
    .module('hiberus-charts')
    .directive('hibPolar', HibPolarDirective);

  HibPolarDirective.$inject = [ '$timeout', 'HibchartsOptions' ];
  
  function HibPolarDirective($timeout, HibchartsOptions) {
    return {
      restrict  : 'E',
      scope     : { categories  : '=',
                    series      : '=',
                    stack       : '=',
                    legend      : '=',
                    animationMs : '=',
                    position    : '@',
                    max         : '=',
                    labelFormatter: '=',
                    background  : '@',
                    colors      : '=',
                    selectable  : '=',
                    selectedPoint: '=',
                    onSelect    : '&',
                    onUnselect  : '&' },
      link      : HibPolarDirectiveLink,
      template  : '<div class="chart" style="position: relative">' + 
                    '<div></div>' + 
                    '<div class="no-data no-data-overview"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
                  '</div>'
    };

    function HibPolarDirectiveLink(scope, element) {

      var chartElement  = element[0].lastChild.firstChild;
      var noDataElement = element[0].lastChild.lastChild;
      var chart = null;

      scope.$watch('series'       , function() { Build(); });
      scope.$watch('categories'   , function() { Build(); });
      scope.$watch('stack'        , function() { Build(); });
      scope.$watch('selectedPoint', function() { SelectPoint(); });
      
      ///////

      function Build() {

        var hasData = scope.series && scope.series.length && scope.series.length > 0;

        var series = JSON.parse(JSON.stringify(scope.series));
        var categories = JSON.parse(JSON.stringify(scope.categories));

        var seriesOptions = {
          animation   : { duration: scope.animationMs ? scope.animationMs : 1000 },
          point       : { events :  { select    : OnSelectHandler,
                                      unselect  : OnUnselectHandler } },
          states      : { select : HibchartsOptions.columnsSelectedStyle } ,
          cursor      : scope.selectable ? 'pointer' : '',
          allowPointSelect: scope.selectable ? true : false 
        };
          
        var options = {
          series      : series,   
          xAxis       : { categories      : categories,
                          labels          : { useHTML: true,
                                              formatter: scope.labelFormatter ? scope.labelFormatter : undefined }, 
                          lineWidth       : 1,
                          gridLineDashStyle: 'longdash' },
          yAxis       : { gridLineInterpolation: 'circle',
                          gridLineDashStyle: 'DashDot',
                          lineWidth       : 1,
                          max             : scope.max ? scope.max : undefined },    
          title       : { text            : '' },
          colors      : scope.colors ? scope.colors : HibchartsOptions.defaultColors,
          chart       : { type            : 'column',
                          backgroundColor : scope.background ? scope.background : 'transparent',
                          polar           : true,
                          marginTop       : 25, 
                          marginLeft      : scope.paginable ? 25 : 25, 
                          marginRight     : scope.position === 'lateral' ? 25 : 25, 
                          marginBottom    : scope.position === 'lateral' ? 25 : 50 },
          exporting   : { enabled         : false },
          credits     : { enabled         : false },
          legend      : { enabled         : scope.legend ? true : false,
                          layout          : scope.position === 'lateral' ? 'vertical' : 'horizontal',                     
                          verticalAlign   : scope.position === 'lateral' ? 'middle'   : 'bottom',
                          align           : scope.position === 'lateral' ? 'right'    : 'center' },
          tooltip     : HibchartsOptions.defaultTooltip,
          plotOptions : { series          : seriesOptions,
                          column          : { stacking  : scope.stack ? 'normal' : false } }
        };

        chart = Highcharts.chart(chartElement, options);
        $(noDataElement).css('display', hasData ? 'none' : 'block');
        SelectPoint();   
      }
     
      function SelectPoint () {
        if (scope.selectedPoint !== undefined && chart !== null && chart.series.length > 0) {
          for (var i = 0; i < chart.series[0].data.length; i += 1) {
            if (chart.series[0].data[i].category === scope.selectedPoint) {
              chart.series[0].data[i].select(true, false);
              return true;
            }
            else if (scope.selectedPoint === null) {
              if (chart.series[0].data[i].selected) {
                chart.series[0].data[i].select(false);
              }
            }
          }
        }

        return false;
      }   

      function OnSelectHandler (event) { 
        if (scope.selectedPoint !== undefined) {
          $timeout(function() {
            scope.selectedPoint = event.target.category;
          }, 100);
        }
        scope.onSelect({ point: { value: event.target.y, name: event.target.series.name, category: event.target.category } }); 
      }

      function OnUnselectHandler (event) {
        if (scope.selectedPoint !== undefined) {
          $timeout(function() {
            scope.selectedPoint = null;
          }, 0);
        }
        scope.onUnselect({ point: { value: event.target.y, name: event.target.series.name, category: event.target.category } }); 
      }
    }
  }  

})();
(function() {

  'use strict';

  angular.module('hiberus-charts').directive('hbRoundProgressColumn', hbRoundProgressColumnDirective);

  function hbRoundProgressColumnDirective() {
    return {
      restrict: 'E',
      link:  {post: Link
            },
      template: '<div class="round-parent-wrapper">'+ 
                  '<div class="round-wrapper round-wrapper-{{length}}"  ng-repeat-n="length">'+
                    '<div class="text" ng-if="valuesRoundProgress[$index].value != null">'+
                      '{{valuesRoundProgress[$index].value>0?\'+\':\'\'}}{{valuesRoundProgress[$index].value | number:0}}%'+
                    '</div>'+
                    '<div ng-if="valuesRoundProgress[$index].value != null" animation-delay="2000" clockwise="valuesRoundProgress[$index].value > 0" color="{{valuesRoundProgress[$index].color}}" current="valuesRoundProgress[$index].value > 0 ? valuesRoundProgress[$index].value : - valuesRoundProgress[$index].value  " fill="#FFFFFF" fill-opacity="0.5" max="100" min="0" offset="0" radius="25" responsive="responsive" round-progress="" rounded="rounded" stroke="5">'+
                    '</div>'+
                  '</div>'+
                '</div>' ,
      scope: {
        valuesRoundProgress           : '=',
        length                        : '='
      },
      replace: true
    };
  }


  function Link (scope, element, attrs) {
  }

})();