(function () {
	
	'use strict';
	
	angular
	.module('hiberus-charts')
	.directive('hibBubbles', HibBubblesDirective);
	
	HibBubblesDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	
	function HibBubblesDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			scope: {
				background: '@',
				hAxisTitle: '@',
				maxX: '=',
				maxY: '=',
				minX: '=',
				minY: '=',
				series: '=',
				labels: '=',
				sizeBubleMin: '=',
				sizeBubleMax: '=',
				tooltip: '=',
				vAxisTitle: '@',
				axisColorX: '=',
				axisColorY: '=',
				gridColorX: '=',
				gridColorY: '=',
				tickLength: '=',
				tickColor: '='
			},
			link: HibBubblesDirectiveLink,
			template: '<div class="chart" style="position: relative">' +
			'<div></div>' +
			'<div class="no-data no-data-bubble"><div class="spnldng"><span class="sfr_span">NO DATA DISPLAY</span><div class="sfr"><div class="int"></div></div></div></div>' +
			'</div>'
		};
		
		function HibBubblesDirectiveLink(scope, element) {
			
			var chartElement = element[0].lastChild.firstChild;
			var noDataElement = element[0].lastChild.lastChild;
			var chart = null;
			var ren, line, area1, area2;
			scope.$watch('series', function () {
				Build();
			});
			scope.$watch('categories', function () {
				Build();
			});
			
			///////
			
			function Build() {
				console.log(scope)
				
				var hasData = scope.series && scope.series.length && scope.series.length > 0;
				
				var series = JSON.parse(JSON.stringify(scope.series));
				//series.push({ name: '', data: [{ x: 0, y: 0, z: 0 }], showInLegend: false, color: 'transparent', enableMouseTracking: false });
				
				var options = {
					chart: {
						type: 'bubble',
						plotBorderWidth: 0,
						backgroundColor: scope.background ? scope.background : 'transparent',
						zoomType: 'xy',
						events: {
							load: function () {
								drawSVG(this, false);
							},
							redraw: function () {
								drawSVG(this, true);
							}
						}
					},
					title: {text: ''},
					exporting: {enabled: false},
					credits: {enabled: false},
					legend: {enabled: false},
					xAxis: {
						lineWidth: 1,
						gridLineWidth: 1,
						max: scope.maxX ? scope.maxX : null,
						min: 0,
						title: {text: scope.hAxisTitle},
						labels: {
							enabled: true,
							format: scope.labels ? scope.labels : "{value}%"
						},
						lineColor: scope.axisColorX ? scope.axisColorX : '#ccd6eb',
						gridLineColor: scope.gridColorX ? scope.gridColorX : '#e6e6e6',
						tickLength: scope.tickLength ? scope.tickLength : 10,
						tickColor: scope.tickColor ? scope.tickColor : '#ccd6eb'
					},
					yAxis: {
						lineWidth: 1,
						gridLineWidth: 1,
						max: scope.maxY ? scope.maxY : null,
						min: 0,
						title: {text: scope.vAxisTitle},
						labels: {
							enabled: true,
							format: scope.labels ? scope.labels : "{value}%"
						},
						lineColor: scope.axisColorY ? scope.axisColorY : '#ccd6eb',
						gridLineColor: scope.gridColorY ? scope.gridColorY : '#e6e6e6'
						
					},
					tooltip: scope.tooltip ? scope.tooltip : HibchartsOptions.defaultTooltip,
					plotOptions: {
						series: {
							dataLabels: {
								enabled: true,
								formatter: function () {
									return this.key
								},
								style: {
									"color": "#ffffff",
									"fontWeight": "100",
									"fontSize": "10px",
									"textShadow": "0 0 6px rgba(0, 0, 0, 0.75), 0 0 3px rgba(0, 0, 0, 0.75)"
								}
							}
						},
						bubble: {
							minSize: scope.sizeBubleMin ? scope.sizeBubleMin : '20',
							maxSize: scope.sizeBubleMax ? scope.sizeBubleMax : '20',
							marker: {lineWidth: 0, fillOpacity: 1, states: {hover: {enabled: false}}}
						}
					},
					series: series
				};
				
				chart = Highcharts.chart(chartElement, options);
				$(noDataElement).css('display', hasData ? 'none' : 'block');
				
				if (!hasData) {
					line.destroy();
					area1.destroy();
					area2.destroy();
					if (!scope.series) {
						$(noDataElement).find('.sfr').css('display', 'none');
						$(noDataElement).find('.sfr_span').css('display', 'block');
					}
				}
			}
			
			function drawSVG(chartElement, destroy) {
				if (destroy) {
					line.destroy();
					area1.destroy();
					area2.destroy();
				}
				ren = chartElement.renderer;
				line = ren.path(['M', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop])
				.attr({
					'stroke-width': 1,
					stroke: 'rgba(128, 128, 128, 0.35)',
					dashstyle: 'dash'
				})
				.add();
				area1 = ren.path(['M', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop, 'L'
					, chartElement.plotLeft, chartElement.plotTop, 'L', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight])
				.attr({
					'stroke-width': 0,
					stroke: 'silver',
					dashstyle: 'dash',
					fill: "rgba(0, 128, 0, 0.3)",
					'fill-opacity': ".2"
				})
				.add();
				area2 = ren.path(['M', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop, 'L'
					, chartElement.plotLeft + chartElement.plotWidth, chartElement.plotTop + chartElement.plotHeight, 'L', chartElement.plotLeft, chartElement.plotTop + chartElement.plotHeight])
				.attr({
					'stroke-width': 0,
					stroke: 'silver',
					dashstyle: 'dash',
					fill: "rgba(255, 0, 0, 0.38)",
					'fill-opacity': ".2"
				})
				.add();
			}
			
		}
	}
	
})();