(function () {

  'use strict';

  angular
    .module('hiberus-charts')
    .factory('HibchartsOptions', HibchartsOptionsFactory);

  function HibchartsOptionsFactory() {

    var columnsSelectedStyle = {
      color: '#fefefe',
      borderColor: 'rgb(124, 181, 236)'
    };

    var donutSelectedStyle = {
      color: 'black',
      borderColor: 'white'
    };

    var semidonutSelectedStyle = {

    };

    var legendItemStyle = {
    };

    var donutDataLabels = {
      color: 'white',
      distance: -25,
      formatter: function () { return Highcharts.numberFormat(this.percentage,1) + '%'; },
      style: {
        textShadow: 'none',
        fontWeight: 'bold'
      }
    };

    var semidonutDataLabels = {
      color: 'white',
      distance: -25,
      formatter: function () { return Highcharts.numberFormat(this.percentage,1) + '%'; },
      style: {
        textShadow: 'none',
        fontWeight: 'bold'
      }
    };

       var PointFormatter = function() {
         var text = Highcharts.numberFormat(this.y, 0);

      if (this.percentage !== undefined) {
        text = Highcharts.numberFormat(this.percentage,1) + '%';
      }

      if (this.z !== undefined) {
        text = '(' + this.x + ', ' + this.y + ') ';
      }

      return '<b>' + text + '</b>';
    }

    var defaultTooltip = {
      enabled: true,
      backgroundColor: '#000',
      borderColor: 'black',
      borderRadius: 10,
      borderWidth: 3,
      style: { color: '#fff', fontWeight: 'bold', fontSize: '12px', fontFamily: 'inherit' },
      pointFormatter: PointFormatter
    };
    
    

    var defaultColors = ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1'];

    return {
      columnsSelectedStyle: columnsSelectedStyle,
      donutSelectedStyle: donutSelectedStyle,
      donutDataLabels: donutDataLabels,
      semidonutSelectedStyle: semidonutSelectedStyle,
      semidonutDataLabels: semidonutDataLabels,
      defaultColors: defaultColors,
      defaultTooltip: defaultTooltip,
      legendItemStyle: legendItemStyle
    };


  }

})();