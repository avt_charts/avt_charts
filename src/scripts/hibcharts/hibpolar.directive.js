(function() { 

  'use strict';

  angular
    .module('hiberus-charts')
    .directive('hibPolar', HibPolarDirective);

  HibPolarDirective.$inject = [ '$timeout', 'HibchartsOptions' ];
  
  function HibPolarDirective($timeout, HibchartsOptions) {
    return {
      restrict  : 'E',
      scope     : { categories  : '=',
                    series      : '=',
                    stack       : '=',
                    legend      : '=',
                    animationMs : '=',
                    position    : '@',
                    max         : '=',
                    labelFormatter: '=',
                    background  : '@',
                    colors      : '=',
                    selectable  : '=',
                    selectedPoint: '=',
                    onSelect    : '&',
                    onUnselect  : '&' },
      link      : HibPolarDirectiveLink,
      template  : '<div class="chart" style="position: relative">' + 
                    '<div></div>' + 
                    '<div class="no-data no-data-overview"><div class="spnldng"><div class="sfr"><div class="int"></div></div></div></div>' +
                  '</div>'
    };

    function HibPolarDirectiveLink(scope, element) {

      var chartElement  = element[0].lastChild.firstChild;
      var noDataElement = element[0].lastChild.lastChild;
      var chart = null;

      scope.$watch('series'       , function() { Build(); });
      scope.$watch('categories'   , function() { Build(); });
      scope.$watch('stack'        , function() { Build(); });
      scope.$watch('selectedPoint', function() { SelectPoint(); });
      
      ///////

      function Build() {

        var hasData = scope.series && scope.series.length && scope.series.length > 0;

        var series = JSON.parse(JSON.stringify(scope.series));
        var categories = JSON.parse(JSON.stringify(scope.categories));

        var seriesOptions = {
          animation   : { duration: scope.animationMs ? scope.animationMs : 1000 },
          point       : { events :  { select    : OnSelectHandler,
                                      unselect  : OnUnselectHandler } },
          states      : { select : HibchartsOptions.columnsSelectedStyle } ,
          cursor      : scope.selectable ? 'pointer' : '',
          allowPointSelect: scope.selectable ? true : false 
        };
          
        var options = {
          series      : series,   
          xAxis       : { categories      : categories,
                          labels          : { useHTML: true,
                                              formatter: scope.labelFormatter ? scope.labelFormatter : undefined }, 
                          lineWidth       : 1,
                          gridLineDashStyle: 'longdash' },
          yAxis       : { gridLineInterpolation: 'circle',
                          gridLineDashStyle: 'DashDot',
                          lineWidth       : 1,
                          max             : scope.max ? scope.max : undefined },    
          title       : { text            : '' },
          colors      : scope.colors ? scope.colors : HibchartsOptions.defaultColors,
          chart       : { type            : 'column',
                          backgroundColor : scope.background ? scope.background : 'transparent',
                          polar           : true,
                          marginTop       : 25, 
                          marginLeft      : scope.paginable ? 25 : 25, 
                          marginRight     : scope.position === 'lateral' ? 25 : 25, 
                          marginBottom    : scope.position === 'lateral' ? 25 : 50 },
          exporting   : { enabled         : false },
          credits     : { enabled         : false },
          legend      : { enabled         : scope.legend ? true : false,
                          layout          : scope.position === 'lateral' ? 'vertical' : 'horizontal',                     
                          verticalAlign   : scope.position === 'lateral' ? 'middle'   : 'bottom',
                          align           : scope.position === 'lateral' ? 'right'    : 'center' },
          tooltip     : HibchartsOptions.defaultTooltip,
          plotOptions : { series          : seriesOptions,
                          column          : { stacking  : scope.stack ? 'normal' : false } }
        };

        chart = Highcharts.chart(chartElement, options);
        $(noDataElement).css('display', hasData ? 'none' : 'block');
        SelectPoint();   
      }
     
      function SelectPoint () {
        if (scope.selectedPoint !== undefined && chart !== null && chart.series.length > 0) {
          for (var i = 0; i < chart.series[0].data.length; i += 1) {
            if (chart.series[0].data[i].category === scope.selectedPoint) {
              chart.series[0].data[i].select(true, false);
              return true;
            }
            else if (scope.selectedPoint === null) {
              if (chart.series[0].data[i].selected) {
                chart.series[0].data[i].select(false);
              }
            }
          }
        }

        return false;
      }   

      function OnSelectHandler (event) { 
        if (scope.selectedPoint !== undefined) {
          $timeout(function() {
            scope.selectedPoint = event.target.category;
          }, 100);
        }
        scope.onSelect({ point: { value: event.target.y, name: event.target.series.name, category: event.target.category } }); 
      }

      function OnUnselectHandler (event) {
        if (scope.selectedPoint !== undefined) {
          $timeout(function() {
            scope.selectedPoint = null;
          }, 0);
        }
        scope.onUnselect({ point: { value: event.target.y, name: event.target.series.name, category: event.target.category } }); 
      }
    }
  }  

})();