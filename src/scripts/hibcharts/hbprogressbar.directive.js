(function () {
	'use strict';
	angular.module('hiberus-charts').directive('hibProgressBar', HibProgressBarDirective);
	
	HibProgressBarDirective.$inject = ['$timeout', 'HibchartsOptions'];
	
	function HibProgressBarDirective($timeout, HibchartsOptions) {
		return {
			restrict: 'E',
			link: HibProgressBarDirectiveLink,
			templateUrl: 'html/doubleprogressbar.template.html',
			scope: {
				series: '=',
				colors: '=',
				prevColumn: '@',
				currentColumn: '@',
				nameCurrentColumn: '@',
				namePrevColumn: '@',
				animationDuration: '@'
			}
		};
	}
	
	
	function HibProgressBarDirectiveLink(scope, element) {
		var chartElement = element[0].firstElementChild.lastElementChild;
		var noDataElement = element[0].lastChild;
		
		var chart = null;
		
		
		scope.$watch('series', function () {
			Build();
		});
		
		function Build() {
			
			var dataSeries = [];
			
			var hasData = scope.series;
			
			var colors = scope.colors ? scope.colors : ['#45CCCE', '#004d45'];
			
			var prevColumn = scope.prevColumn ? scope.prevColumn : 'spend';
			var currentColumn = scope.currentColumn ? scope.currentColumn : 'budget';
			var namePrevColumn = scope.namePrevColumn ? scope.namePrevColumn : 'Total Spend';
			var nameCurrentColumn = scope.nameCurrentColumn ? scope.nameCurrentColumn : 'Total Budget';
			
			if (scope.series) {
				scope.valueRoundProgress = ((scope.series[currentColumn] - scope.series[prevColumn]) / scope.series[currentColumn]) * 100;
				
				var obj = {
					name: namePrevColumn,
					data: [scope.series[prevColumn]],
					pointWidth: 6,
					series: {
						pointPadding: 15,
						groupPadding: 10
					},
					borderWidth: 0,
					animation: {
						duration: scope.animationDuration !== undefined ? parseInt(scope.animationDuration) : 3000,
					}
				};
				dataSeries.push(obj);
				var obj = {
					name: nameCurrentColumn,
					data: [scope.series[currentColumn]],
					pointWidth: 6,
					series: {
						pointPadding: 15,
						groupPadding: 10
					},
					borderWidth: 0,
					animation: {
						duration: scope.animationDuration !== undefined ? parseInt(scope.animationDuration) : 3000
					}
				};
				dataSeries.push(obj);
				
			}
			
			var chartOptions = {
				chart: {
					type: 'bar',
					margin: [80, 10, 55, 0],
					backgroundColor: null,
					events: {
						load: function () {
							addRoundProgress();
						}
					}
				},
				colors: colors,
				tooltip: {
					enabled: false
				},
				title: {
					text: null
				},
				legend: {
					symbolWidth: 3,
					symbolHeight: 3,
					symbolRadius: 5,
					y: 0,
					enabled: true,
					floating: false,
					verticalAlign: 'top',
					align: 'left',
					layout: 'horizontal',
					useHTML: true,
					labelFormatter: function () {
						return '<div><span style="float:left;clear:both;display:block;font-size:.8rem!important;font-family:Gotham-Light !important;text-transform: uppercase;">' + this.name + '</span><span style="float:left;clear:both;display:blockfont-size:12px;font-family:Gotham !important">' + this.yData[0] + '</span></div>';
					}
				},
				exporting: {enabled: false},
				credits: {
					enabled: false
				},
				xAxis: {
					lineWidth: 1
				},
				yAxis: {
					gridLineWidth: 1,
					lineWidth: 0,
					min: 0,
					title: {
						enabled: false
					},
					labels: {
						enabled: true
					}
				},
				series: dataSeries
			};
			
			
			chart = Highcharts.chart(chartElement, chartOptions);
			$(noDataElement).css('display', hasData ? 'none' : 'block');
			
			
			scope.showChart = true;
			scope.isLoading = false;
		}
		
		function addRoundProgress() {
			scope.showRoundProgress = true;
		}
	}
})();